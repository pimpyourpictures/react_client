import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import reduxPromise from 'redux-promise';
import logger from 'redux-logger';
import { BrowserRouter as Router, Route, Link, Match, Redirect, Switch } from 'react-router-dom';
import { createHistory as history } from 'history';

import reducers from "./reducers";

import { PublicRoute } from './layouts';
import Public404 from './layouts/public/404';

import SearchBar from "./components/search_bar";
import TemplateSelectinTopbar from "./components/template_selection_topbar";

import './assets/stylesheets/application.scss';

const middlewares = applyMiddleware(reduxPromise, logger);

// render an instance of the component in the DOM
ReactDOM.render(
  <Provider store={createStore(reducers, {}, middlewares)}>
    <Router history={history}>
      <Switch>
        <PublicRoute exact path="/" title="Choose Style" active_menu="choose-templates" main_component={SearchBar} topbar_component={TemplateSelectinTopbar}/>
        <Route exact path="/choose-templates" render={() => (<Redirect to="/" />)}/>
        <PublicRoute exact path="/video-details" title="video-details" active_menu="video-details" main_component={SearchBar} topbar_component={TemplateSelectinTopbar}/>
        <PublicRoute exact path="/add-assets" title="add-assets" active_menu="add-assets" main_component={SearchBar} topbar_component={TemplateSelectinTopbar}/>
        <PublicRoute exact path="/watch-preview" title="watch-preview" active_menu="watch-preview" main_component={SearchBar} topbar_component={TemplateSelectinTopbar}/>
        <PublicRoute exact path="/thank-you" title="thank-you" active_menu="thank-you" main_component={SearchBar} topbar_component={TemplateSelectinTopbar}/>
        <PublicRoute title="Page not found" main_component={Public404} topbar_component={TemplateSelectinTopbar}/>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);
