import React from "react";
import { Component } from "react";
import { BrowserRouter as Router, Route, Link, Match, Redirect, Switch } from 'react-router-dom';

import logo from '../../assets/img/logo.png';
import jessicaDoe from '../../assets/img/jessica-doe.png';
import playPreview from '../../assets/img/play-preview.png';
import abstractThumb from '../../assets/img/thumbs/abstract-thumb.jpg';
import christmasTree from '../../assets/img/thumbs/christmas-tree.jpg';
import marryChrismats from '../../assets/img/thumbs/marry-chrismats.jpg';
import winterHouse from '../../assets/img/thumbs/winter-house.jpg';
import happyNewYear from '../../assets/img/thumbs/happy-new-year.jpg';
import businessProject from '../../assets/img/thumbs/business-project.jpg';
import finishedProject from '../../assets/img/finished-project.png';
import style1 from '../../assets/img/styles-images/style-1.jpg';
import savedProject from '../../assets/img/saved-project.png';
import style3 from '../../assets/img/styles-images/style-3.jpg';
import style4 from '../../assets/img/styles-images/style-4.jpg';
import style6 from '../../assets/img/styles-images/style-6.jpg';
import style7 from '../../assets/img/styles-images/style-7.jpg';
import style8 from '../../assets/img/styles-images/style-8.jpg';
import girlBig from '../../assets/img/girl-big.jpg';
import template1 from '../../assets/img/template-previews/template-1.jpg';
import template2 from '../../assets/img/template-previews/template-2.jpg';
import template3 from '../../assets/img/template-previews/template-3.jpg';
import template4 from '../../assets/img/template-previews/template-4.jpg';
import template5 from '../../assets/img/template-previews/template-5.jpg';
import template6 from '../../assets/img/template-previews/template-6.jpg';

export default class MainMenu extends Component {
  render() {
    return (
      <aside>
        <div className="logo">
          <Link to="#">
            <img src={logo} />
          </Link>
        </div>
        <nav className="sidemenu">
          <ul>
            <li>
              <Link className={this.props.active_menu=="choose-templates" ? "active" : ""} to="choose-templates" />
            </li>
            <li>
              <Link className={this.props.active_menu=="video-details" ? "active" : ""} to="video-details" />
            </li>
            <li>
              <Link className={this.props.active_menu=="add-assets" ? "active" : ""} to="add-assets" />
            </li>
            <li>
              <Link className={this.props.active_menu=="watch-preview" ? "active" : ""} to="watch-preview" />
            </li>
            <li>
              <Link className={this.props.active_menu=="thank-you" ? "active" : ""} to="thank-you" />
            </li>
          </ul>
        </nav>
        <div className="pagination">1/5</div>
      </aside>
    );
  }
}
