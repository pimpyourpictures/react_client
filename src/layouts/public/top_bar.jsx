import React from "react";
import { Component } from "react";
import { BrowserRouter as Router, Route, Link, Match, Redirect, Switch } from 'react-router-dom';

export default class TopBar extends Component {
  render() {
    return (
      <header>
        {this.props.children}
      </header>
    );
  }
}
