import React from "react";
import { Component } from "react";
import { BrowserRouter as Router, Route, Link, Match, Redirect, Switch } from 'react-router-dom';

import logo from '../../assets/img/logo.png';
import jessicaDoe from '../../assets/img/jessica-doe.png';
import playPreview from '../../assets/img/play-preview.png';
import abstractThumb from '../../assets/img/thumbs/abstract-thumb.jpg';
import christmasTree from '../../assets/img/thumbs/christmas-tree.jpg';
import marryChrismats from '../../assets/img/thumbs/marry-chrismats.jpg';
import winterHouse from '../../assets/img/thumbs/winter-house.jpg';
import happyNewYear from '../../assets/img/thumbs/happy-new-year.jpg';
import businessProject from '../../assets/img/thumbs/business-project.jpg';
import finishedProject from '../../assets/img/finished-project.png';
import style1 from '../../assets/img/styles-images/style-1.jpg';
import savedProject from '../../assets/img/saved-project.png';
import style3 from '../../assets/img/styles-images/style-3.jpg';
import style4 from '../../assets/img/styles-images/style-4.jpg';
import style6 from '../../assets/img/styles-images/style-6.jpg';
import style7 from '../../assets/img/styles-images/style-7.jpg';
import style8 from '../../assets/img/styles-images/style-8.jpg';
import girlBig from '../../assets/img/girl-big.jpg';
import template1 from '../../assets/img/template-previews/template-1.jpg';
import template2 from '../../assets/img/template-previews/template-2.jpg';
import template3 from '../../assets/img/template-previews/template-3.jpg';
import template4 from '../../assets/img/template-previews/template-4.jpg';
import template5 from '../../assets/img/template-previews/template-5.jpg';
import template6 from '../../assets/img/template-previews/template-6.jpg';

export default class ProfileMenu extends Component {
  constructor(props) {
    super(props);

    this.state = { opened: false };
  }
  render() {
    return (
      <div className={this.state.opened ? 'menu-open' : ''} >
        <div className="profile">
          <Link to="#" className="profile-name">Jessica Doe</Link>
          <img src={jessicaDoe} />
          <span className="user-name">Jessica Doe</span>
          <span className="current-balance">- 400</span>
        </div>
        <div className="menu-icon" onClick={() => this.setState({opened: !this.state.opened})}>
          <span className="menu-line-top" />
          <span className="menu-line-mid" />
          <span className="menu-line-bot" />
        </div>
        <section className="profile-menu">
          <ul>
            <li>
              <Link to="choose-styles.html">Create video</Link>
            </li>
            <li>
              <Link to="#" className="my-projects">My Projects</Link>
            </li>
            <li>
              <Link to="#" className="purchased-videos">Purchased Videos</Link>
            </li>
            <li>
              <Link to="#" className="favorite-styles">Favorite Styles</Link>
            </li>
            <li>
              <Link to="#" className="my-wallet">My Wallet</Link>
            </li>
            <li>
              <Link to="#" className="settings">Settings</Link>
            </li>
            <li>
              <Link to="#" className="aaaaaa">Logout</Link>
            </li>
          </ul>
        </section>
      </div>
    );
  }
}
