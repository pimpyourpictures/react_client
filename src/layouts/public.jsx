import React from 'react';
import { BrowserRouter as Router, Route, Link, Match, Redirect, Switch } from 'react-router-dom';

import Empty from "../utils/empty";
import SetTitle from "../utils/set_title";

import MainMenu from "./public/main_menu";
import TopBar from "./public/top_bar";
import ProfileMenu from "./public/profile_menu";

import logo from '../assets/img/logo.png';
import jessicaDoe from '../assets/img/jessica-doe.png';
import playPreview from '../assets/img/play-preview.png';
import abstractThumb from '../assets/img/thumbs/abstract-thumb.jpg';
import christmasTree from '../assets/img/thumbs/christmas-tree.jpg';
import marryChrismats from '../assets/img/thumbs/marry-chrismats.jpg';
import winterHouse from '../assets/img/thumbs/winter-house.jpg';
import happyNewYear from '../assets/img/thumbs/happy-new-year.jpg';
import businessProject from '../assets/img/thumbs/business-project.jpg';
import finishedProject from '../assets/img/finished-project.png';
import style1 from '../assets/img/styles-images/style-1.jpg';
import savedProject from '../assets/img/saved-project.png';
import style3 from '../assets/img/styles-images/style-3.jpg';
import style4 from '../assets/img/styles-images/style-4.jpg';
import style6 from '../assets/img/styles-images/style-6.jpg';
import style7 from '../assets/img/styles-images/style-7.jpg';
import style8 from '../assets/img/styles-images/style-8.jpg';
import girlBig from '../assets/img/girl-big.jpg';
import template1 from '../assets/img/template-previews/template-1.jpg';
import template2 from '../assets/img/template-previews/template-2.jpg';
import template3 from '../assets/img/template-previews/template-3.jpg';
import template4 from '../assets/img/template-previews/template-4.jpg';
import template5 from '../assets/img/template-previews/template-5.jpg';
import template6 from '../assets/img/template-previews/template-6.jpg';



const PublicLayout = ({ children, active_menu, title, ...rest }) => {
  return (
    <div className="page-choose-template">
      <MainMenu active_menu={active_menu} />
      <TopBar>
        {children.filter( (comp) => {return comp.key === "topbar";})}
      </TopBar>
      <ProfileMenu />
      <form method="post" action="video-details.html">
        <main>
          {children.filter( (comp) => {return comp.key === "main";})}
        </main>
        <footer>
          <button className="cancel" type="cancel">Cancel</button>
          <button id="next" className="next" type="submit">Next</button>
        </footer>
      </form>
      <SetTitle title={title} />
    </div>
  )
}

const PublicRoute = ({ active_menu, title, main_component: Component, topbar_component: TopBarComponent, ...rest }) => {
  return (
    <Route {...rest} render={matchProps => (
      <PublicLayout active_menu={active_menu} title={title}>
        <Component {...matchProps} key="main" />
        <TopBarComponent {...matchProps} key="topbar" />
      </PublicLayout>
    )} />
  )
};

export default PublicRoute;
