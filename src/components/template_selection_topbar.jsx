import React from "react";
import { Component } from "react";

export default class TemplateSelectinTopbar extends Component {
  render() {
    return (
      <nav className="menu">
        <div className="controls">
          <button type="button" data-filter="all">All</button>
          <button type="button" data-filter=".logo-reveal">Logo reveal</button>
          <button type="button" data-filter=".slideshow">Slideshow</button>
          <button type="button" data-filter=".explainer">Explainer videos</button>
          <button type="button" data-filter=".promote">Promote</button>
          <button type="button" data-filter=".real-estate">Real estate</button>
        </div>
      </nav>
    );
  }
}
