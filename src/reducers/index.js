import { combineReducers } from "redux";
import ActiveBook from "./reducer_active_book";

const rootReducer = combineReducers({
  activeBook: ActiveBook
});

export default rootReducer;
